﻿import tkinter as tk
from src import gui

# Run the tool
if __name__ == '__main__':
  window = tk.Tk()
  gui_obj = gui.BattleFacilityToolGUI(window)
  gui_obj.create_widgets()
  gui_obj.display()
  window.mainloop()
