﻿import csv
from src.poke_utils import *
from src.utils import *

class StatsDict(dict):
  """
  This is a wrapper class for a regular Python dictionary that represents a set
  of 6 values associated to each stat. It can be used for actual stat values,
  IVs, EVs, etc. It can be initialized with a string such as '0/252/0/0/6/252'
  or with a list of 6 values such as [31, 0, 31, 31, 31, 31]. Values can be
  accessed with obj['hp'], obj['def'], etc, like in a regular dict.
  """
  def __init__(self, values):
    if isinstance(values, str):
      values = values.split('/')
    if len(values) != 6:
      raise ValueError(f"Argument {values} must have 6 elements, or be a "
        "string with 6 values separated by '/'")
    for val in values:
      if not is_valid_num(val):
        raise ValueError(f"Element {val} is not a positive int or string "
          "convertible to it")
    for key, val in zip(STATS, values):
      self[key] = int(val)

  def to_string(self):
    """Returns values in portable '/'-separated string format."""
    return '/'.join([str(_) for _ in self.values()])


class Pokemon(dict):
  """
  This is a wrapper class for a regular Python dictionary that represents a
  Pokémon. It works identical to a normal dict, but it has a handful of new
  useful methods and tweaks for this particular task. For example, you can also
  set and access dict values with dot notation, for example poke.item instead
  of poke['item']. Morover there are methods to quickly access the Pokémon's
  stats and species name, or correctly set IVs/EVs. Finally, at construction,
  if there are any 'evs' and 'ivs' args and they are not StatsDict objects
  already, they are turned into ones, if possible.
  """
  def __init__(self, *args, **kwargs):
    # Call regular dict constructor
    super().__init__(*args, **kwargs)
    # Turn IVs and EVs into StatsDicts
    if 'evs' in self and self.evs != '' and not isinstance(self.evs,StatsDict):
      self.set_evs(self.evs)
    if 'ivs' in self and self.evs != '' and not isinstance(self.ivs,StatsDict):
      self.set_ivs(self.ivs)

  def __getattr__(self, key):
    """Allows dot notation to access dict fields."""
    return self[key]

  def __setattr__(self, key, val):
    """Allows dot notation to set dict fields."""
    self[key] = val

  def get_full_species_name(self):
    """Returns the original species name only, including Mega Evo suffix.
    This function assumes that the Pokémon has a 'name' field. It removes the
    set suffix from it, if any, and adds the Mega Evolution suffix to the name,
    if the Pokémon's 'item' field is a Mega Stone. If none of this changes can
    be made, the original name is returned. Note that for the purposes of Mega
    Evolution, this function assumes that the item is the appropriate Mega
    Stone for the Pokémon, as it only checks whether the item is a Mega Stone
    or not, regardless of it being the correct one or not."""
    # Mega Evolution with particular Mega suffixes
    special_megas = {
      'Charizardite X': 'Charizard-Mega-X',
      'Charizardite Y': 'Charizard-Mega-Y',
      'Mewtwonite X': 'Mewtwo-Mega-X',
      'Mewtwonite Y': 'Mewtwo-Mega-Y'
    }
    name, _ = split_poke_name_and_set_suffix(self.name)
    # Check if it holds a Mega Stone
    if 'item' not in self or self.item == 'Eviolite':
      return name
    elif self.item.endswith('ite'):
      return name + '-Mega'
    # Deal with special Megas
    elif self.item in special_megas:
      return special_megas[self.item]
    else:
      return name

  def fetch_basestats(self):
    """Searches and sets its base stats in the appropriate database.
    This assumes that the Pokémon has a 'name' and a 'generation' field, so
    that the corresponding database can be accessed. If not, an error will be
    raised. ValueError is also raised if the Pokémon name is not found in the
    database. If the search is successful, the base stats will be saved as a
    StatsDict object in the 'basestats' field.
    """
    name = self.get_full_species_name()
    with open(f'data/stats/gen{self.generation}.csv') as f:
      read = csv.DictReader(f)
      for poke in read:
        if name == poke['name']:
          self.basestats = StatsDict([int(poke[_]) for _ in STATS])
          return
    raise ValueError(f"{name} is not a valid Pokémon name")

  def get_stats(self):
    """Returns its stat values for the current Pokémon settings.
    This function assumes that the Pokémon has appropriate 'level', 'ivs',
    'evs', and 'nature' members. If the 'basestats' member is not present,
    it will be created via a call to fetch_basestats(). The returned stat
    values are not saved as an object member.
    """
    if 'stats' not in self:
      self.fetch_basestats()
    stats = []
    for s in STATS:
      stats.append(stat_calculation_formula(s, self.basestats[s], self.level,
        self.ivs[s], self.evs[s], self.nature))
    return StatsDict(stats)

  def set_evs(self, evs):
    """Sets the Pokémon's 'evs' field.
    The field is always set as a StatsDict object. If the argument is a
    StatsDict object, it is set as-is, otherwise the method will try to
    inizialize one with the given argument.
    """
    if isinstance(evs, StatsDict):
      self.evs = evs
    else:
      self.evs = StatsDict(evs)

  def set_ivs(self, ivs):
    """Sets the Pokémon's 'ivs' field.
    The field is always set as a StatsDict object. If the argument is a
    StatsDict object, it is set as-is, otherwise the method will try to
    inizialize one with the given argument. Unlike set_evs(), this method also
    works if the argument is a single valid number (or string convertible to
    it): in this case, all 6 IVs will be set equal to that value.
    """
    if isinstance(ivs, StatsDict):
      self.ivs = ivs
    elif is_valid_num(ivs):
      self.ivs = StatsDict(6*[int(ivs)])
    else:
      self.ivs = StatsDict(ivs)
