﻿from src.utils import *

# Pokémon-related constants
STATS = ('hp', 'atk', 'def', 'spa', 'spd', 'spe')
GEN1_DVS = ('hp', 'atk', 'def', 'spe', 'spa')
NATURES_TABLE = {
  'Lonely':  [1.1, 0.9, 1.0, 1.0, 1.0],
  'Brave':   [1.1, 1.0, 1.0, 1.0, 0.9],
  'Adamant': [1.1, 1.0, 0.9, 1.0, 1.0],
  'Naughty': [1.1, 1.0, 1.0, 0.9, 1.0],
  'Bold':    [0.9, 1.1, 1.0, 1.0, 1.0],
  'Docile':  [1.0, 1.0, 1.0, 1.0, 1.0],
  'Relaxed': [1.0, 1.1, 1.0, 1.0, 0.9],
  'Impish':  [1.0, 1.1, 0.9, 1.0, 1.0],
  'Lax':     [1.0, 1.1, 1.0, 0.9, 1.0],
  'Timid':   [0.9, 1.0, 1.0, 1.0, 1.1],
  'Hardy':   [1.0, 1.0, 1.0, 1.0, 1.0],
  'Hasty':   [1.0, 0.9, 1.0, 1.0, 1.1],
  'Serious': [1.0, 1.0, 1.0, 1.0, 1.0],
  'Jolly':   [1.0, 1.0, 0.9, 1.0, 1.1],
  'Naive':   [1.0, 1.0, 1.0, 0.9, 1.1],
  'Modest':  [0.9, 1.0, 1.1, 1.0, 1.0],
  'Mild':    [1.0, 0.9, 1.1, 1.0, 1.0],
  'Quiet':   [1.0, 1.0, 1.1, 1.0, 0.9],
  'Bashful': [1.0, 1.0, 1.0, 1.0, 1.0],
  'Rash':    [1.0, 1.0, 1.1, 0.9, 1.0],
  'Calm':    [0.9, 1.0, 1.0, 1.1, 1.0],
  'Gentle':  [1.0, 0.9, 1.0, 1.1, 1.0],
  'Sassy':   [1.0, 1.0, 1.0, 1.1, 0.9],
  'Careful': [1.0, 1.0, 0.9, 1.1, 1.0],
  'Quirky':  [1.0, 1.0, 1.0, 1.0, 1.0]
  }

def stat_calculation_formula(stat, base, level, iv, ev, nature):
  """Returns stat value given the base stat and all other relevant values.
  If the args are not valid values, ValueErrors are raised."""
  if stat not in STATS:
    raise ValueError(f"{stat} is not in {STATS}")
  if nature not in NATURES_TABLE:
    raise ValueError(f"{nature} is not a valid Nature")
  for x in (base, level, iv, ev):
    if not is_valid_num(x):
      raise ValueError(f"{x} is not a valid number")
  base = int(base)
  level = int(level)
  iv = int(iv)
  ev = int(ev)
  nat_mult = get_nature_multiplier(nature, stat)
  if stat == 'hp':
    return int((2*base + iv + int(ev/4))*level/100) + level + 10
  else:
    return int( (int((2*base + iv + int(ev/4))*level/100)+5)*nat_mult )

def split_poke_name_and_set_suffix(name):
  """Returns two strings: Pokémon name and set name.
  The original name and '' are resturned if no valid suffix is found. A valid
  suffix is one that can be found in a battle facility Pokémon set name."""
  SETS_SUFFIXES = [ str(_) for _ in range(20+1)] + ['H', 'R']
  for suf in SETS_SUFFIXES:
    if name.endswith('-'+suf):
      return name.rstrip('-'+suf), suf
  return name, ''

def get_nature_multiplier(nature, stat):
  """Return 0.9, 1.0, 1.1 based on the given stat name and nature.
  If the args are not valid values, ValueErrors are raised. A correct
  lower-case nature is accepted, though."""
  nature = nature.title()
  if stat not in STATS:
    raise ValueError(f"{stat} is not in {STATS}")
  if nature not in NATURES_TABLE:
    raise ValueError(f"{nature} is not a valid Nature")
  if stat == 'hp':
    return 1.0
  else:
    return NATURES_TABLE[nature][STATS.index(stat)-1]
