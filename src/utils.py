﻿# String utility
def replace_slice_in_string(strg, index, substrg):
  """Replaces substring at position index of strg.
  It returns the original strg if substrg would end up out of range."""
  if index+len(substrg) > len(strg):
    return strg
  else:
    return ''.join((strg[:index], substrg, strg[index+len(substrg):]))

# String constants
SEP_LINE = 114*'-' + '\n'
POKE_LINE = replace_slice_in_string(SEP_LINE, 5, " POKÉMON ")
TRAINER_LINE = replace_slice_in_string(SEP_LINE, 5, " TRAINER ")
DMG_CALC_LINE = replace_slice_in_string(SEP_LINE, 5, " DAMAGE CALCULATION ")

# Pokémon-related constants
GENERATIONS = [_ for _ in range(1,8+1)]
GAMES_GENERATIONS = {
  'rb': 1, 'rbg': 1, 'rby': 1, 'rbgy': 1,
  'gs': 2, 'c': 2, 'gsc': 2,
  'rs': 3, 'rse': 3, 'frlg': 3, 'e': 3,
  'dp': 4, 'dpp': 4, 'dppt': 4, 'pt': 4, 'hgss': 4,
  'bw': 5, 'b2w2': 5, 'bw2': 5,
  'xy': 6, 'oras': 6,
  'sm': 7, 'usum': 7, 'usm': 7,
  'swsh': 8
}

# Other utilities
def is_valid_num(num):
  """Checks if num is a valid positive int or string convertible to it."""
  return (isinstance(num, int) and num >= 0) or \
    (isinstance(num, str) and num.isdigit())

def correct_status(status):
  """Converts abbreviations for status conditions into full strings.
  Conversion is case-insensitive with respect to the input. If the input is not
  a valid value, the original string will be returned."""
  if status.lower() == 'asleep':
    return 'slp'
  elif status.lower() == 'burned':
    return 'brn'
  elif status.lower() == 'frozen':
    return 'frz'
  elif status.lower() == 'paralyzed':
    return 'par'
  elif status.lower() == 'poisoned':
    return 'psn'
  else:
    return status

def correct_weather(weather):
  """Converts abbreviations for weathers into full strings with correct case.
  Conversion is case-insensitive with respect to the input. If the input is not
  a valid value, the capitalized original string will be returned."""
  if weather.lower() == 'sandstorm':
    return 'Sand'
  else:
    return weather.title()
