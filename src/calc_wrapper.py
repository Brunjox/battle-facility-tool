﻿import json
import subprocess
from src.utils import *

class CalcWrapperClass:
  """
  This is a Python wrapper class for Smogon's damage calculator, which is
  written in JavaScript. Its sole purpose is to store the four Python dicts
  relevant for damage calculation, translate them into JSON dicts, pass them to
  the calculator, and run it. The Python dicts can be passed either to the
  class constructor, or to the setter. This class has a string output buffer
  which stores the output of the calculator, which can be printed later by
  accessing the buffer.
  """
  def __init__(self, gen=8, attacker=None, defender=None, move=None,
    field=None):
    # Initialize field names
    self.KEYS = ('attacker', 'defender', 'move', 'field')
    # Prepare fields
    self.clear_all()
    # Set values if present
    self.set_generation(gen)
    if attacker:
      self.set_object('attacker', attacker)
    if defender:
      self.set_object('defender', defender)
    if move:
      self.set_object('move', move)
    if field:
      self.set_object('field', field)

  def set_generation(self, gen):
    self.generation = gen

  def set_object(self, key, dictionary):
    """Set the given dictionary to one of the valid keys.
    Valid keys are 'attacker', 'defender', 'move', 'field'. If not valid, the
    function will not do anything."""
    if key not in self.KEYS:
      return
    self.all_dicts[key] = dictionary

  def output(self, *args):
    """Stores printable text into the output buffer."""
    self.output_buffer += ''.join(args)

  def clear_output(self):
    """Clears the output buffer."""
    self.output_buffer = ""

  def clear_all(self):
    """Clears all fields of the class."""
    self.clear_output()
    self.generation = None
    self.all_dicts = dict.fromkeys(self.KEYS, None)

  def calc(self):
    """Calls JS calculator with the given dicts and stores its output.
    This function needs all four objects (attacker, defender, move, field) as
    well as the generation. If any of them is missing, ValueErrors are raised.
    """
    self.clear_output()
    if not self.generation:
      ValueError("Generation value is missing")
      return
    for key, val in self.all_dicts.items():
      if not val:
        ValueError(f"{key} object is missing")
        return
    # Correct stupid Flabébé case
    if self.all_dicts['attacker']['name'] == 'Flabébé':
      self.all_dicts['attacker']['name'] = 'Flabebe'
    if self.all_dicts['defender']['name'] == 'Flabébé':
      self.all_dicts['defender']['name'] = 'Flabebe'
    # Create JSONs
    attacker_json = json.dumps(self.all_dicts['attacker'])
    defender_json = json.dumps(self.all_dicts['defender'])
    move_json = json.dumps(self.all_dicts['move'])
    field_json = json.dumps(self.all_dicts['field'])
    # Call Smogon JavaScript file
    args = ('node', 'src/calc_smogon.js', str(self.generation), attacker_json,
      defender_json, move_json, field_json)
    # Get and store output
    run_js = subprocess.run(args, stdout=subprocess.PIPE)
    self.output(run_js.stdout.decode(), SEP_LINE)
