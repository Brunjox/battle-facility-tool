﻿import csv
from src.poke_utils import *
from src.utils import *

class DisplayLibClass:
  """
  This is a class that displays Pokémon and Trainer information with two
  separate methods. It stores the printed text in a string output buffer rather
  than printing it right away. The information printed also depends on the
  game, which is passed to the class constructor.
  """
  def __init__(self, game):
    # Initialize relevant objects
    self.game = game
    self.gen = GAMES_GENERATIONS[game]
    self.clear_output()

  def output(self, *args):
    """Stores printable text into the output buffer."""
    self.output_buffer += ''.join(args)

  def clear_output(self):
    """Clears the output buffer."""
    self.output_buffer = ""

  def display_trainer(self, trainer):
    """Displays all information contained in the given Trainer.
    Such information includes the full Trainer roster with all possible Pokémon
    sets and Trainer-specific notes, if any.
    """
    # Clear buffer and print title
    self.clear_output()
    self.output(TRAINER_LINE)
    # Print Trainer-specific notes
    if self.game == 'pt':
      if trainer['name_eng'].startswith('Darach'):
        self.output("NOTE: This Trainer can use two different teams (A or B)"
          "\n\n")
      elif trainer['name_eng'].startswith('Thorton'):
        self.output("NOTE: This Trainer's team also varies depending on the "
          "game and challenge level. For example, if you wish to search for "
          "the Gold Print (i.e. 2nd fight), Level 100 challenge team in "
          "HGSS, please add \"2-100-hgss\" to the Trainer name\n\n")
    elif self.game == 'swsh' and trainer['dynamax_list'] != '':
      self.output("NOTE: This Trainer can Dynamax the following Pokémon: ",
        trainer['dynamax_list'], "\n\n")
    # Prepare printing of Trainer roster
    pokes_sets = dict()
    for poke in sorted(trainer['pokemon'].split('/')):
      name, set_ = split_poke_name_and_set_suffix(poke)
      if name not in pokes_sets:
        pokes_sets[name] = [set_]
      else:
        pokes_sets[name] += [set_]
    # Print Trainer
    self.output(f"#{trainer['no'].zfill(3)} {trainer['name']}")
    if 'class' in trainer:
      self.output(f" ({trainer['class']})")
    self.output(": ")
    self.output(', '.join([p + '(' + '-'.join(pokes_sets[p]) + ')'
      for p in pokes_sets]), "\n")

  def display_pokes(self, matches_names, matches_pokemon):
    """Displays all information on the given Pokémon.
    matches_names is a list of Pokémon set names to keep track in which order
    they will be printed. matches_pokemon is a dict which associates each name
    to its corresponding Pokémon object."""
    # Clear buffer and print title
    self.clear_output()
    self.output(POKE_LINE)
    # Initialize paddings dict
    all_keys = 'no name sex item nature moves'.split()
    pokes_keys = list(matches_pokemon.values())[0].keys()
    paddings_keys = []
    for key in all_keys:
      # get only keys that are in the Pokémon
      if key in pokes_keys:
        paddings_keys.append(key)
    paddings = dict.fromkeys(paddings_keys, 0)
    # Get paddings
    for poke in matches_pokemon.values():
      for key in paddings_keys:
        paddings[key] = max(paddings[key], len(poke[key]))
    # Print Diamond/Pearl nature notice
    if self.game == 'dp':
      self.output("NOTE: in this game, Pokémon NATURES are RANDOM rather than "
        "fixed. If a nature is not provided, a neutral nature will be used "
        "for Speed and damage calculations.\n\n")
    # Print Pokémon: each field has a padding
    for name in matches_names:
      poke = matches_pokemon[name]
      self.output(f"#{poke.no.rjust(paddings['no'])}"
                  f" {poke.name.ljust(paddings['name'])}")
      if 'item' in poke:
        self.output(f" @ {poke.item.ljust(paddings['item'])}")
      if 'nature' in poke:
        self.output(f" - {poke.nature.ljust(paddings['nature'])}")
      self.output(f" - {poke.moves.ljust(paddings['moves'])}")
      # Compute Speed stat
      if self.game == 'c':
        # Get the raw Speed stat
        speed = poke.stats.split('/')[5].rjust(3)
      else:
        # Get Speed stat
        fullname = poke.get_full_species_name()
        speed = poke.get_stats()['spe']
        # Add Choice Scarf boost
        if poke.item == 'Choice Scarf':
          speed = int(1.5*speed)
      # Print Speed stat
      self.output(f" - {str(speed).rjust(3)} Spe")
      # Print EVs
      if self.game != 'c':
        self.output(f" - EV {poke.evs.to_string()}\n")
      else:
        self.output("\n")
