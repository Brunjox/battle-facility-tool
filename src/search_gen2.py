﻿import csv
from src import search
from src.pokemon import *
from src.utils import *

class Gen2SearchLibClass(search.SearchLibClass):
  """
  This is a class for searching Trainers and Pokémon in the database files of
  the Battle Tower in Pokémon Crystal. It is built as a derived class of
  SearchLibClass because of the very different mechanics of this facility (and
  structure of its corresponding database) with respect to all other ones.
  In particular, rosters depend only on the challenge level rather than on the
  identity of the faced Trainers. Therefore, the Trainer input is interpreted
  as a challenge level, which can range from 10-100 with step 10. For more
  information, check the base class SearchLibClass.
  """
  def __init__(self):
    # Initialize database files and constants
    self.pokesfile = 'data/c_pokemon.csv'
    self.mypokesfile = 'my_pokemon.csv'
    self.gen = 2
    self.CHALLENGE_LEVELS = [str(10*_) for _ in range(1,11)]
    self.clear_all()

  def get_roster(self, level):
    """Returns the roster for the given challenge level."""
    roster = []
    with open(self.pokesfile, 'r', encoding='utf-8') as f:
      read = csv.DictReader(f)
      for poke in read:
        if poke['level'] == level:
          roster.append(Pokemon(poke))
    return roster

  def search(self, trainer_input, pokes_raw_input):
    """Searches Trainer and returns Pokémon that match the inputs.
    This is the main function of the class, and it operates similarly to its
    base-class counterpart, except for the fact that it displays the roster
    directly rather than delegate the printing of the Trainer to the
    DisplayClass, since there is no actual Trainer to print."""
    # Clear buffer
    self.clear_output()
    # Check if the input is valid
    level = str(trainer_input)
    if level not in self.CHALLENGE_LEVELS:
      self.output(SEP_LINE,
        "Error: please provide a value among 10, 20, ..., 100\n", SEP_LINE)
      return [], {}
    # Get roster
    pokes_input_list = self.parse_pokes_input(pokes_raw_input)
    roster = self.get_roster(level)
    roster_names = sorted([p.name for p in roster])
    self.output(TRAINER_LINE, f"Level {level} challenge: ",
      ', '.join(roster_names), "\n")
    # Get pokes
    if not pokes_raw_input:
      matches_names = roster_names
      matches_pokemon = dict.fromkeys(roster_names, Pokemon())
      # Get all pokes for that level
      for poke in roster:
        matches_pokemon[poke.name] = Pokemon(poke)
    else:
      # Get only pokes that match
      matches_names = []
      matches_pokemon = {}
      empty = True
      for inp in pokes_input_list:
        for poke in roster:
          if self.poke_name_matches(poke.name, inp):
            empty = False
            matches_names.append(poke.name)
            matches_pokemon[poke.name] = Pokemon(poke)
      if empty:
        self.output(POKE_LINE, "Error: no matching Pokémon found\n",
          SEP_LINE)
        return [], {}
    return matches_names, matches_pokemon
