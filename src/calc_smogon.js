"use strict";
exports.__esModule = true;
var smogoncalc = require("@smogon/calc");

// Parse arguments
var gen = Number(process.argv[2]);
var attacker_json = JSON.parse(process.argv[3]);
var defender_json = JSON.parse(process.argv[4]);
var move_json = JSON.parse(process.argv[5]);
var field_json = JSON.parse(process.argv[6]);

// Print title
console.log([attacker_json.nickname, " vs ", defender_json.nickname,
  ":"].join(''));

// Initialize objects
try {
  var attacker_obj = new smogoncalc.Pokemon(gen, attacker_json.name,
    attacker_json);
}
catch(err) {
  console.log(["Error: attacker \"", attacker_json.name, "\" is not valid"
    ].join(''));
  process.exit();
}
try {
  var defender_obj = new smogoncalc.Pokemon(gen, defender_json.name,
    defender_json);
}
catch(err) {
  console.log(["Error: defender \"", defender_json.name, "\" is not valid"
    ].join(''));
  process.exit();
}
var move_obj = new smogoncalc.Move(gen, move_json.name, move_json);
if (!move_obj.type){
  console.log(["Error: move \"", move_json.name, "\" not found"].join(''));
  process.exit();
}
var field_obj = new smogoncalc.Field(field_json);

// Perform damage calculation
var result = smogoncalc.calculate(gen, attacker_obj, defender_obj, move_obj,
  field_obj);
// console.log(result);
var desc = result.rawDesc;
delete desc.isDefenderDynamaxed;
if (!desc.isBurned){
  delete desc.isBurned;
}

// Add Levels
if(attacker_obj.level == defender_obj.level){
  desc.level = attacker_obj.level;
}
else{
  desc.level = [attacker_obj.level, 'vs', defender_obj.level].join(' ');
}

// Add Speed comparison
if(gen != 2){
  desc.speeds = [result.attacker.stats.spe, 'vs',
                 result.defender.stats.spe].join(' ');
}

// OUTPUT CREATION
if (!result.damage){
  console.log("no damage lol");
  process.exit();
}
// Damage rolls
var minDmg = result.damage[0].toString();
var maxDmg = result.damage[15].toString();
var defHP = result.defender.rawStats.hp;
var minPerc = (100 * minDmg / defHP).toFixed(1);
var maxPerc = (100 * maxDmg / defHP).toFixed(1);
var percent = minPerc + ' - ' + maxPerc + '%';
var count = 0;
var factor = (maxDmg >= defHP) ? 1 : 2;
for(var i = 0; i < result.damage.length; i++){
  if(factor * result.damage[i] >= defHP){
    count++;
  }
}
// Summarized string of result
var description = factor + 'HKO: ';
var rolls = result.damage.length;
if(count == rolls){
  description += 'always';
}
else if(count == 0){
  description += 'never';
}
else{
  description += [count, rolls].join(" / ");
}
var dmgStrg = result.damage.join(", ");
// Damage dictionary
var dmgDict = { percent: percent, description: description, values: dmgStrg,
  hp: defHP };

// Print message
// console.log(result);
console.log(desc);
console.log(dmgDict);
