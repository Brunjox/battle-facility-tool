﻿from math import ceil, sqrt
from src import search, search_gen2, display, calc_wrapper
from src.pokemon import *
from src.utils import *

class ToolInterfaceClass:
  """
  This is the core class of the tool. It turns user-produced gibberish into
  actually useful objects, while creating and communicating with all other
  classes of the library by storing them as class members. Its main method,
  run(), executes the searching and damage calculation of the tool. It can
  be run both as a GUI via the BattleFacilityToolGUI class, and as a
  command-line interface. This class has a string output buffer which stores
  the output of the calculator, which can be printed later by accessing the
  buffer.
  """
  def __init__(self, game, language):
    # Create database names
    pokesfile = f'data/{game}_pokemon.csv'
    trainersfile = f'data/{game}_trainers.csv'
    mypokesfile = 'my_pokemon.csv'
    # Create library objects
    if game == 'c':
      self.search_obj = search_gen2.Gen2SearchLibClass()
    else:
      self.search_obj = search.SearchLibClass(game, language, pokesfile,
        trainersfile, mypokesfile)
    self.display_obj = display.DisplayLibClass(game)
    self.calc_obj = calc_wrapper.CalcWrapperClass()
    # Initialize values
    self.game = game
    self.clear_all()

  def output(self, *args):
    """Stores printable text into the output buffer."""
    self.output_buffer += ''.join(args)

  def clear_output(self):
    """Clears the output buffer."""
    self.output_buffer = ""

  def clear_all(self):
    """Clears all fields of the class, except the ones set at construction."""
    self.attacker = Pokemon()
    self.defender = Pokemon()
    self.move_dict = {}
    self.field_dict = {}
    self.clear_output()

  def set_evs_ivs_gen2(self, poke):
    """Returns updated Pokémon with EVs/IVs converted from stat exp/DVs.
    poke is a Pokémon object. This function assumes that it contains the
    'statexp' and 'dvs' fields. The conversion is only an approximation for
    the purposes of passing the Pokémon to the Smogon damage calculator."""
    # Convert stat experience to EVs
    if poke.statexp == '':
      # Default to max stat experience
      poke.statexp = '/'.join(5*['65535'])
    evs = [min(255, ceil(sqrt(int(s)))) for s in poke.statexp.split('/')]
    poke.evs = dict(zip(GEN1_DVS, evs))
    poke.evs['spd'] = poke.evs['spa']
    # Convert DVs to IVs
    ivs = [2*int(d) for d in poke.dvs.split('/')]
    poke.ivs = dict(zip(GEN1_DVS, ivs))
    poke.ivs['spd'] = poke.ivs['spa']
    return poke

  def produce_attacker_and_defender(self, my_pokemon, opp_pokemon,
    my_poke_is_attacker, move, is_z_move, is_max_move, crit, att_boosts,
    def_boosts, weather, terrain, att_status, def_status, opp_ability, screens,
    my_level, opp_level):
    """Creates and stores proper attacker and defender objects from input args.
    my_pokemon and opp_pokemon are Pokémon objects, and are the starting points
    for the construction of attacker and defender, which are then stored as
    class members. The other args should be self-explanatory; see the
    BattleFacilityToolGUI class for more information."""
    # Set opponent's fields
    opp_pokemon.nickname = opp_pokemon.name
    opp_pokemon.name = opp_pokemon.get_full_species_name()
    opp_pokemon.level = int(opp_level)
    if opp_ability:
      opp_pokemon.ability = opp_ability.title()
    # Set my poke's fields
    my_pokemon.name = my_pokemon.get_full_species_name()
    my_pokemon.level = int(my_level)
    # Create attacker and defender objects
    if my_poke_is_attacker:
      self.attacker = my_pokemon
      self.defender = opp_pokemon
    else:
      self.attacker = opp_pokemon
      self.defender = my_pokemon
    # Set EVs and IVS in Crystal
    if self.game == 'c':
      self.attacker = self.set_evs_ivs_gen2(self.attacker)
      self.defender = self.set_evs_ivs_gen2(self.defender)
    # Set status conditions
    self.attacker.status = correct_status(att_status)
    self.defender.status = correct_status(def_status)
    # Set stat boosts
    self.attacker.boosts = {'atk': int(att_boosts),
      'spa': int(att_boosts)}
    self.defender.boosts = {'def': int(def_boosts),
      'spd': int(def_boosts)}
    # Create move dict
    self.move_dict = dict(name=move, isCrit=crit, useZ=is_z_move,
      useMax=is_max_move)
    # Create field dict
    self.field_dict = dict(weather=correct_weather(weather), terrain=terrain)
    self.field_dict['defenderSide'] = dict(isReflect=screens,
      isLightScreen=screens)

  def run(self, trainer_input, pokes_input, do_damage_calc, my_poke_input,
    my_poke_is_attacker, move, is_z_move, is_max_move, crit, att_boosts,
    def_boosts, weather, terrain, att_status, def_status, opp_ability,
    screens, my_level, opp_level, opp_nature):
    """Runs the tool: Pokémon searching and damage calculation (if enabled).
    This is the main function of the class: it uses all initialized objects and
    allows communication among them. trainer_input and pokes_input are
    the strings that will be searched in the Trainer and Pokémon databases.
    Damage calculation is only performed if the do_damage_calc flag is set to
    True. All other arguments are the same as the ones for
    produce_attacker_and_defender(). Output from this function is stored in the
    output buffer."""
    # Clear buffer
    self.clear_output()
    # Search
    self.matches_names, self.matches_pokemon = \
      self.search_obj.search(trainer_input, pokes_input)
    self.output(self.search_obj.output_buffer)
    if not self.matches_pokemon:
      return
    # Display Trainer if present
    if hasattr(self.search_obj, 'trainer') and self.search_obj.trainer:
      self.display_obj.display_trainer(self.search_obj.trainer)
      self.output(self.display_obj.output_buffer)
    # Check whether to update Trainer IVs
    if hasattr(self.search_obj,'trainer') and 'ivs' in self.search_obj.trainer:
      ivs = self.search_obj.trainer['ivs']
    else:
      ivs = -1
    # Check whether to update nature with input nature in Diamond/Pearl
    if self.game == 'dp' and opp_nature.title() in NATURES_TABLE:
      nat = opp_nature.title()
    else:
      nat = None
    # Update matching Pokémon
    for poke in self.matches_pokemon.values():
      poke.level = opp_level
      if ivs != -1:
        poke.set_ivs(ivs)
      elif 'ivs' not in poke:
        # default to perfect IVs
        poke.set_ivs(31)
      if nat is not None:
        poke.nature = nat
    # Display pokes
    self.display_obj.display_pokes(self.matches_names, self.matches_pokemon)
    self.output(self.display_obj.output_buffer)
    if not do_damage_calc:
        self.output(SEP_LINE)
        return
    else:
      # Damage calculation
      self.output(DMG_CALC_LINE)
      # Check that both custom poke and move inputs are not null
      if not my_poke_input or not move:
        x = "your Pokémon" if not my_poke_input else "a move"
        self.output(f"Please select {x}!\n", SEP_LINE)
        return
      # Search custom poke
      my_pokemon = self.search_obj.find_poke_in_database(my_poke_input, True)
      if not my_pokemon:
        self.output("Error: your Pokémon was not found\n", SEP_LINE)
        return
      # Set levels to challenge level in Crystal
      if self.game == 'c':
        my_level = trainer_input
        opp_level = trainer_input
      # Get opponent Pokémon
      opp_pokemon = self.matches_pokemon[ self.matches_names[-1] ]
      # Produce attacker and defender dict objects
      self.produce_attacker_and_defender(my_pokemon, opp_pokemon,
        my_poke_is_attacker, move, is_z_move, is_max_move, crit, att_boosts,
        def_boosts, weather, terrain, att_status, def_status, opp_ability,
        screens, my_level, opp_level)
      # Assign dicts to wrapper object and perform damage calculation
      self.calc_obj.set_generation(GAMES_GENERATIONS[self.game])
      self.calc_obj.set_object('attacker', self.attacker)
      self.calc_obj.set_object('defender', self.defender)
      self.calc_obj.set_object('move', self.move_dict)
      self.calc_obj.set_object('field', self.field_dict)
      self.calc_obj.calc()
      self.output(self.calc_obj.output_buffer)
