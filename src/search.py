﻿import csv
from src.pokemon import *
from src.utils import *

class SearchLibClass:
  """
  This is a class for searching Trainers and Pokémon in the battle facilities'
  database files, whose names are given at construction. It searches a Trainer
  whose name in the indicated language (also provided at construction) matches
  the given input, then searches all Pokémon of that Trainer that match the
  given inputs. If no Trainer input is given, the entire Pokémon database will
  be searched. Prefixing ! to the given name demands an exact match of the
  Trainer name: for example, input "!jim" matches with Trainer Jim but not with
  Jimmy. The matching Trainer is stored as the trainer class member, and the
  matching Pokémon are returned via the matches_names and matches_pokemon (see
  the docstring of search() for more information). This class has a string
  output buffer which stores any potential search errors, which can be printed
  later by accessing the buffer. Other than that, the displaying of the search
  results is delegated to the DisplayClass.
  """
  def __init__(self, game, language, pokesfile, trainersfile, mypokesfile):
    """Last 3 args are csv database file names."""
    # Initialize database filenames and other relevant values
    self.gen = GAMES_GENERATIONS[game]
    self.language = language
    self.pokesfile = pokesfile
    self.trainersfile = trainersfile
    self.mypokesfile = mypokesfile
    self.clear_all()

  def parse_pokes_input(self, input_):
    """Returns an input list from the given input string, splitting on spaces.
    If there are duplicate inputs, the earlier ones are removed."""
    pokes_input_list = []
    for poke in reversed(input_.lower().split()):
      if poke not in pokes_input_list:
        pokes_input_list.append(poke)
    pokes_input_list.reverse()
    return pokes_input_list

  def poke_name_matches(self, name, input_name):
    """Checks if a Pokémon name matches the input string (case-insensitive).
    Spaces are removed from the original Pokémon name during the comparison."""
    name = name.lower().replace(' ', '')
    input_ = input_name.lower()
    return name.startswith(input_)

  def find_trainer_in_database(self, trainer_input):
    """Returns a Trainer from the game database that macthes the input name.
    An empty dict is returned if none is found. If trainer_input starts with !,
    an exact match of the name is demanded. The Trainer name is searched in
    the game and language that were given as arguments to the class
    constructor."""
    # Initialize exact match flag
    exact_match = False
    if trainer_input.startswith('!'):
      trainer_input = trainer_input[1:]
      exact_match = True
    # Open database and loop over Trainers
    with open(self.trainersfile, 'r', encoding='utf-8') as f:
      reader = csv.DictReader(f)
      trainer_input = trainer_input.lower()
      # Select correct language
      key = 'name_' + self.language
      for trainer in reader:
        # Decide type of match
        if exact_match:
          match = (trainer[key].lower() == trainer_input)
        else:
          match = (trainer[key].lower().startswith(trainer_input))
        if match:
          trainer['name'] = trainer[key]
          return trainer
      return {}

  def find_poke_in_database(self, input_name, use_my_database=False):
    """Returns a Pokémon from the game database that macthes the input name.
    An empty Pokémon object is returned if none is found or if the input is
    null. If use_my_database is True, a Pokémon with a matching 'nickname' will
    be searched in the custom Pokémon database instead. A 'generation' field is
    added to the returned Pokémon, if any. Please note that this function is
    not used in the search() method for efficiency reasons."""
    if not input_name:
      return Pokemon()
    # Choose database and field
    if use_my_database:
      file = self.mypokesfile
      field = 'nickname'
    else:
      file = self.pokesfile
      field = 'name'
    # Open database and loop over pokes
    with open(file, 'r', encoding='utf-8') as f:
      reader = csv.DictReader(f)
      for poke in reader:
        if self.poke_name_matches(poke[field], input_name):
          return Pokemon(poke, generation=self.gen)
    return Pokemon()

  def output(self, *args):
    """Stores printable text into the output buffer."""
    self.output_buffer += ''.join(args)

  def clear_output(self):
    """Clears the output buffer."""
    self.output_buffer = ""

  def clear_all(self):
    """Clears all fields of the class, except the ones set at construction."""
    self.clear_output()
    self.trainer = {}

  def find_pokes_in_trainer(self, trainer, pokes_input_list):
    """Returns a list and dict of matching Pokémon from the given Trainer.
    pokes_input_list is a list of not-necessarily-complete Pokémon input names.
    For an explanation of return values, see the docstring of search().
    Here a 'generation' field is added to the returned Pokémon, if any."""
    if not pokes_input_list:
      # Create universally matching input
      pokes_input_list = ['']
    # Create Trainer roster list
    trainer_roster = sorted(trainer['pokemon'].split('/'))
    # Initialize temporary auxiliary dict of matches' lists and name list
    names_lists_by_input = dict.fromkeys(pokes_input_list, [])
    matches_names = []
    # Fill names_lists_by_input by looping on both Trainer and input Pokémon
    for inp_poke in pokes_input_list:
      for tr_poke in trainer_roster:
        if self.poke_name_matches(tr_poke, inp_poke):
          # Append Pokémon to the corresponding lists
          names_lists_by_input[inp_poke] = \
            names_lists_by_input[inp_poke] + [tr_poke]
          matches_names.append(tr_poke)
    # Check if all input pokes were found
    num_matches = sum(bool(_) for _ in names_lists_by_input.values())
    if num_matches != len(pokes_input_list):
      return [], {}
    # Open database and loop over pokes to get matching ones
    matches_pokemon = dict.fromkeys(matches_names, Pokemon())
    with open(self.pokesfile, 'r', encoding='utf-8') as f:
      reader = csv.DictReader(f)
      for poke in reader:
        if poke['name'] in matches_names:
          matches_pokemon[poke['name']] = Pokemon(poke, generation=self.gen)
    return matches_names, matches_pokemon

  def search_pokes_without_trainer(self, pokes_input_list):
    """Returns a list and dict of matching Pokémon from the entire database.
    pokes_input_list is a list of not-necessarily-complete Pokémon input names.
    For an explanation of return values, see the docstring of search().
    Here a 'generation' field is added to the returned Pokémon, if any."""
    # Initialize return values and temporary auxiliary dict of matches lists
    matches_names = []
    matches_pokemon = {}
    names_lists_by_input = dict.fromkeys(pokes_input_list, [])
    # Open database and both database and input pokes
    with open(self.pokesfile, 'r', encoding='utf-8') as f:
      reader = csv.DictReader(f)
      for poke in reader:
        for inp_poke in pokes_input_list:
          if self.poke_name_matches(poke['name'], inp_poke):
            # Append poke to the corresponding lists
            names_lists_by_input[inp_poke] = \
              names_lists_by_input[inp_poke] + [poke['name']]
            matches_pokemon[ poke['name'] ] = Pokemon(poke,
              generation=self.gen)
    # Fill matches_names
    for p in pokes_input_list:
      matches_names.extend(names_lists_by_input[p])
    matches_names.sort()
    return matches_names, matches_pokemon

  def search(self, trainer_input, pokes_raw_input):
    """Searches Trainer and returns Pokémon that match the inputs.
    This is the main function of the class. Return values are: matches_names,
    a list of Pokémon names to keep track in which order they will be printed,
    and matches_pokemon, a dict which associates each name to its corresponding
    Pokémon object. If no match is found, or if there is not at least one match
    per input, an empty list and dict are returned."""
    # Clear buffers
    self.clear_all()
    # Check there is at least one valid input
    if not trainer_input and not pokes_raw_input:
      self.output(SEP_LINE, "Please select Pokémon and/or a Trainer!\n",
        SEP_LINE)
      return [], {}
    # Initialize objects
    pokes_input_list = self.parse_pokes_input(pokes_raw_input)
    matches_names = []
    matches_pokemon = {}
    # Trainer search mode
    if trainer_input:
      self.trainer = self.find_trainer_in_database(trainer_input)
      if not self.trainer:
        self.output(TRAINER_LINE,
          f"Error: Trainer {trainer_input} was not found\n", SEP_LINE)
        return [], {}
      else:
        matches_names, matches_pokemon = \
          self.find_pokes_in_trainer(self.trainer, pokes_input_list)
        if not matches_pokemon: # should be the same as with matches_names
          self.output(TRAINER_LINE, f"Error: Trainer {self.trainer['name']} ",
            "was found, but the given Pokémon do not match\n", SEP_LINE)
          return [], {}
    else: # if not trainer_input
      # Pokémon-only search mode
      matches_names, matches_pokemon = \
        self.search_pokes_without_trainer(pokes_input_list)
      if not matches_names:
        self.output(POKE_LINE, "Error: no matching Pokémon found\n", SEP_LINE)
        return [], {}
    return matches_names, matches_pokemon
