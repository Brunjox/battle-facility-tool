from src import search

class BattleFactoryToolInterfaceClass:
  def __init__(self, gen, level, language):
    # Initialize relevant constants and values
    GENS_TO_GAMES = {3: 'e', 4: 'pt'} 
    if isinstance(level, str) and level.lower() == 'open':
      level = 100
    if gen not in GENS_TO_GAMES or level not in (50, 100):
      return
    self.gen = gen
    self.level = level
    game = GENS_TO_GAMES[gen]
    # Create databases names
    pokesfile = f'data/{game}_pokemon.csv'
    if self.gen == 3:
      trainersfile = f'data/{game}_factory.csv'
    else:
      trainersfile = f'data/{game}_trainers.csv'
    self.search_obj = search.SearchLibClass(game, language, pokesfile,
      trainersfile, '')

  def get_roster(self, input_):
    trainer = self.search_obj.find_trainer_in_database(input_)
    roster = trainer['factory_'+str(self.level)].split('/')
    print(roster)
    return roster
