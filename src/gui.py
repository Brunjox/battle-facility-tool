﻿from configobj import ConfigObj
import Pmw
import tkinter as tk
from src import tool
from src.utils import *

class BattleFacilityToolGUI:
  """
  This is a class that displays the Battle Facility Tool in Guided User
  Interface (GUI) form. It reads fixed parameters from CONFIG.txt, if it
  exists, and shows widgets that the user can read, click, or write into. The
  user can search both Pokémon and Trainer names from any of the implemented
  battle facilities. They can also perform damage calculations with one of
  their own Pokémon, read from the appropriate .csv files. On the right side,
  the results of the search and damage calculation, if enabled, are shown.
  """
  def read_config(self, configfile):
    """Reads configuration file, if any, and sets parameters from it."""
    # Read file and params, if present
    config = ConfigObj(configfile)
    game = config.get('game')
    lang = config.get('language')
    level = config.get('level')
    # Set values, if valid
    if lang and lang.lower() in self.LANGUAGES:
      self.language = lang.lower()
    if game and game.lower() in self.GAMES:
      self.game = game.lower()
    if self.game in ('e', 'pt') and level.isdigit() and \
      int(level) in range(50, 100+1):
      self.level = level

  def __init__(self, window):
    """Creates a GUI based on the given tkinter window."""
    self.window = window
    # Supported games and languages
    self.FULL_GAMES = {
      'c': "Pokémon Crystal",
      'e': "Pokémon Emerald",
      'dp': "Pokémon Diamond/Pearl",
      'pt': "Pokémon Platinum/HeartGold/SoulSilver",
      'bw': "Pokémon Black/White/Black2/White2",
      'xy': "Pokémon X/Y",
      'oras': "Pokémon Omega Ruby/Alpha Sapphire",
      'sm': "Pokémon Sun/Moon",
      'usum': "Pokémon Ultra Sun/Ultra Moon",
      'swsh': "Pokémon Sword/Shield"
    }
    self.GAMES = self.FULL_GAMES.keys()
    self.LANGUAGES = ('deu', 'eng', 'esp', 'fra', 'ita')
    # Set default values
    self.language = 'eng'
    self.game = 'usum'
    self.level = 50
    # Set values from configuration file
    self.read_config('CONFIG.txt')
    # Set generation
    self.generation = GAMES_GENERATIONS[self.game]
    # Initialize other constants
    self.STATUSES = ("none", "Asleep", "Burned", "Frozen", "Paralyzed",
      "Poisoned")
    self.WEATHERS = ("none", "Hail", "Rain", "Sandstorm", "Sun")
    self.TERRAINS = ("none", "Electric", "Grassy", "Misty", "Psychic")
    self.dmg_calc_widgets = []
    # Create interface object
    self.interface_obj = tool.ToolInterfaceClass(self.game, self.language)

  def create_widgets(self):
    """Creates all widget objects without displaying them."""
    # Initialize and display window
    Pmw.initialise(self.window)
    self.window.title("Battle Facility Tool for Pokémon by Brunjox")
    width = int(0.99*self.window.winfo_screenwidth())
    height = int(0.9*self.window.winfo_screenheight())
    self.window.geometry(f'{width}x{height}')
    p = tk.PhotoImage(file='data/icons/tool.png')
    self.window.iconphoto(False, p)
    # Config summary
    config_string = ("Game: "
      f"{self.FULL_GAMES[self.game]} ({self.language.upper()})")
    if self.generation in (3, 4):
      config_string += f", Level {self.level}"
    self.config_options_text = tk.Label(self.window, text=config_string)
    self.config_info = Pmw.Balloon(self.window)
    config_info_strg = ("You can choose game, language of Trainer names, and "
      "Level (when applicable) by editing CONFIG.txt")
    if self.game in ('e', 'pt'):
      x = " and Battle Hall" if self.game == 'pt' else ""
      config_info_strg += ("\nNOTE: Pokémon in the earlier rounds of the "
        f"Battle Factory{x} may have slightly different\nIVs, therefore "
        "possibly producing some small errors in the Speed values shown and "
        "in damage calculations")
    self.config_info.bind(self.config_options_text, config_info_strg)
    # Trainer name
    self.trainer_text = tk.Label(self.window, text="Trainer name ")
    if self.game == 'c':
      self.trainer_text.config(text="Challenge Lv ")
    self.trainer_box = tk.Entry(self.window, width=40)
    self.trainer_info = Pmw.Balloon(self.window)
    trainer_strg = ("For an exact match of the name use the ! prefix, for "
      "example: !jim")
    if self.game != 'swsh':
      trainer_strg += ("\nIf left blank, Pokémon will match regardless of "
        "their Trainer, and perfect IVs will be\nassumed for them")
    trainer_strg += ("\nNOTE: in several games and languages, a small handful "
      "of Trainers have the same name.\nThese are marked with numbers to "
      "distinguish them, for example: Leslie 1 and Leslie 2")
    if self.game == 'c':
       trainer_strg = "Choose 10, 20, ..., or 100"
    elif self.game == 'e':
      trainer_strg += ("\nNOTE: in the Battle Factory, only the Frontier "
        "Brain has a fixed roster, thus please leave\nthis field blank unless "
        "you're battling that Brain")
    elif self.game == 'pt':
      trainer_strg += ("\nNOTE: in the Battle Factory and Battle Hall, only "
        "the Frontier Brains have a fixed roster,\nthus please leave this "
        "field blank unless you're battling those Brains")
    self.trainer_info.bind(self.trainer_box, trainer_strg)
    # Opponent pokes
    self.poke_text = tk.Label(self.window, text="List of Pokémon ")
    self.poke_box = tk.Entry(self.window, width=40)
    self.poke_info = Pmw.Balloon(self.window)
    poke_strg = ("Entries must be separated by spaces. If a Pokémon name "
      "includes a space, write the name\nwithout spaces, for example: mr.mime")
    if self.game == 'pt':
      poke_strg += ("\nNOTE: Pokémon used in the Battle Hall are denoted by "
        "names ending in H, for example: Weavile-H")
    if self.game == 'swsh':
      poke_strg += ("\nNOTE: Pokémon used in Restricted Sparring are denoted "
        "by names ending in R, for example: Weavile-R")
    self.poke_info.bind(self.poke_box, poke_strg)
    # Damage calculation button
    self.dmg_calc_frame = tk.Frame(self.window)
    self.dmg_calc_var = tk.BooleanVar()
    self.dmg_calc_text = tk.Label(self.dmg_calc_frame,
      text="Perform damage calculation? ")
    self.dmg_calc_button = tk.Checkbutton(self.dmg_calc_frame,
      variable=self.dmg_calc_var, command=self.toggle_damage_calculation)
    self.dmg_calc_info = Pmw.Balloon(self.dmg_calc_frame)
    dmg_calc_strg = "The opposing Pokémon used will be the last Pokémon shown"
    if self.game == 'c':
      dmg_calc_strg += ("\nNOTE: in Generation 2, calculations may produce "
        "some small errors due to the use of an\napproximated formula")
    self.dmg_calc_info.bind(self.dmg_calc_button, dmg_calc_strg)
    # Opponent level (displayed in Pt/HGSS only, used for Battle Hall)
    self.opp_lvl_frame = tk.Frame(self.window)
    self.opp_lvl_text = tk.Label(self.opp_lvl_frame, text="Opp. lv. ")
    self.opp_lvl_var = tk.StringVar(value=self.level)
    self.boosts_frame = tk.Frame(self.opp_lvl_frame)
    self.opp_lvl_box = tk.Spinbox(self.opp_lvl_frame, from_=10, to=100,
      textvariable=self.opp_lvl_var, width=4)
    self.opp_lvl_info = Pmw.Balloon(self.opp_lvl_frame)
    self.opp_lvl_info.bind(self.opp_lvl_box, "For the Battle Hall")
    # Oppnent nature (displayed in DP only)
    self.opp_nature_frame = tk.Frame(self.window)
    self.opp_nature_text = tk.Label(self.opp_nature_frame, text="Opp.nature")
    self.opp_nature_box = tk.Entry(self.opp_nature_frame, width=8)
    self.opp_nature_info = Pmw.Balloon(self.opp_nature_frame)
    self.opp_nature_info.bind(self.opp_nature_box,
      "Since natures are random in DP Battle Tower.\n"
      "Leave blank for a neutral nature")
    # User's poke
    self.my_poke_text = tk.Label(self.window, text="My Pokémon ",
      state='disabled')
    self.my_poke_box = tk.Entry(self.window, state='disabled', width=16)
    self.my_poke_info = Pmw.Balloon(self.window)
    self.my_poke_info.bind(self.my_poke_box,
      "Write the nickname entered in the .csv file")
    self.my_poke_att_frame = tk.Frame(self.window)
    self.my_poke_att_var = tk.BooleanVar(value=True)
    self.my_poke_att_text = tk.Label(self.my_poke_att_frame, text="is",
      state='disabled')
    self.my_poke_att_button1 = tk.Radiobutton(self.my_poke_att_frame,
      text="attacking", variable=self.my_poke_att_var, value=True,
      state='disabled')
    self.my_poke_att_button2 = tk.Radiobutton(self.my_poke_att_frame,
      text="defending", variable=self.my_poke_att_var, value=False,
      state='disabled')
    self.dmg_calc_widgets.extend([self.my_poke_text, self.my_poke_box,
      self.my_poke_att_text, self.my_poke_att_button1,
      self.my_poke_att_button2])
    # Move name
    self.move_text = tk.Label(self.window, text="Move used ", state='disabled')
    self.move_box = tk.Entry(self.window, state='disabled', width=16)
    self.move_frame = tk.Frame(self.window)
    # Z-Move button
    self.z_move_var = tk.BooleanVar()
    self.z_move_text = tk.Label(self.move_frame, text="Z-Move?", width=7,
      state='disabled')
    self.z_move_button = tk.Checkbutton(self.move_frame, state='disabled',
      variable=self.z_move_var)
    self.z_move_info = Pmw.Balloon(self.move_frame)
    self.z_move_info.bind(self.z_move_button, "Transforms a regular move "
      "into its corresponding Z-Move (Generation 7 only)")
    self.crit_var = tk.BooleanVar()
    # Max Move button (replaces the above in Gen8 only)
    self.max_move_var = tk.BooleanVar()
    self.max_move_text = tk.Label(self.move_frame, text="      Max?", width=7,
      state='disabled')
    self.max_move_button = tk.Checkbutton(self.move_frame, state='disabled',
      variable=self.max_move_var)
    self.max_move_info = Pmw.Balloon(self.move_frame)
    self.max_move_info.bind(self.max_move_button, "Transforms a regular move "
      "into its corresponding Max Move (Generation 8 only)")
    self.crit_var = tk.BooleanVar()
    # Critical hit button
    self.crit_text = tk.Label(self.move_frame, text="Critical hit?",
      state='disabled')
    self.crit_button = tk.Checkbutton(self.move_frame, state='disabled',
      variable=self.crit_var)
    # Extend widgets list
    self.dmg_calc_widgets.extend([self.move_text, self.move_box,
      self.crit_text, self.crit_button])
    # Attacker stat boosts
    self.att_boosts_text = tk.Label(self.window, text="Boosts: attacker ",
      state='disabled')
    self.att_boosts_var = tk.StringVar(value="0")
    self.boosts_frame = tk.Frame(self.window)
    self.att_boosts_box = tk.Spinbox(self.boosts_frame, from_=-6, to=6,
      textvariable=self.att_boosts_var, width=3, state='disabled')
    # Defender stat boosts
    self.def_boosts_var = tk.StringVar(value="0")
    self.def_boosts_text = tk.Label(self.boosts_frame, text="defender ",
      state='disabled', width=9, anchor='e')
    self.def_boosts_box = tk.Spinbox(self.boosts_frame, from_=-6, to=6,
      textvariable=self.def_boosts_var, width=3, state='disabled')
    # Extend widgets list
    self.dmg_calc_widgets.extend([self.att_boosts_text, self.att_boosts_box,
      self.def_boosts_text, self.def_boosts_box])
    # Field conditions:
    self.field_frame = tk.Frame(self.window)
    # Weather
    self.weather_var = tk.StringVar()
    self.weather_text = tk.Label(self.window, text="Weather ",
      state='disabled')
    self.weather_frame = tk.Frame(self.field_frame)
    self.weather_box = tk.Listbox(self.weather_frame, selectmode=tk.SINGLE,
      listvariable=self.weather_var, height=1, width=10)
    for i in range(len(self.WEATHERS)):
      self.weather_box.insert(i+1, self.WEATHERS[i])
    self.weather_box.config(state='disabled')
    self.weather_scroll = tk.Scrollbar(self.weather_frame,
      command=self.weather_box.yview)
    # Terrain
    self.terrain_var = tk.StringVar()
    self.terrain_text = tk.Label(self.field_frame, text="Terrain ", width=9,
      state='disabled', anchor='e')
    self.terrain_frame = tk.Frame(self.field_frame)
    self.terrain_box = tk.Listbox(self.terrain_frame, selectmode=tk.SINGLE,
      listvariable=self.terrain_var, height=1, width=10)
    if self.generation >= 6:
      for i in range(len(self.TERRAINS)):
        self.terrain_box.insert(i+1, self.TERRAINS[i])
      if self.generation == 6:
        self.terrain_box.delete(4)  # Psychic Terrain only exists in Gen 7+
    else:
      self.terrain_box.insert(1, self.TERRAINS[0])
    self.terrain_box.config(state='disabled')
    self.terrain_info = Pmw.Balloon(self.terrain_frame)
    self.terrain_info.bind(self.terrain_box, "Generations 6+ only")
    self.terrain_scroll = tk.Scrollbar(self.terrain_frame,
      command=self.terrain_box.yview)
    # Extend widgets list
    self.dmg_calc_widgets.extend([self.weather_text, self.weather_box,
      self.terrain_text, self.terrain_box])
    # Status conditions:
    self.status_frame = tk.Frame(self.window)
    # Attacker status condition
    self.att_status_var = tk.StringVar()
    self.att_status_text = tk.Label(self.window, text="Status: attacker ",
      state='disabled')
    self.att_status_box = tk.Listbox(self.status_frame,
      listvariable=self.att_status_var, height=1, width=10)
    for i in range(len(self.STATUSES)):
      self.att_status_box.insert(i+1, self.STATUSES[i])
    self.att_status_box.config(state='disabled')
    self.att_status_scroll = tk.Scrollbar(self.status_frame,
      command=self.att_status_box.yview)
    # Defender status condition
    self.def_status_var = tk.StringVar()
    self.def_status_text = tk.Label(self.status_frame, text="defender ",
      width=9, state='disabled', anchor='e')
    self.def_status_box = tk.Listbox(self.status_frame,
      listvariable=self.def_status_var, height=1, width=10)
    for i in range(len(self.STATUSES)):
      self.def_status_box.insert(i+1, self.STATUSES[i])
    self.def_status_box.config(state='disabled')
    self.def_status_scroll = tk.Scrollbar(self.status_frame,
      command=self.def_status_box.yview)
    # Extend widgets list
    self.dmg_calc_widgets.extend([self.att_status_text,
      self.att_status_box, self.def_status_text, self.def_status_box])
    # Opponent's ability
    self.other_options_frame = tk.Frame(self.window)
    self.opp_abil_text = tk.Label(self.window,
      text="Opponent ability ", state='disabled')
    self.opp_abil_box = tk.Entry(self.other_options_frame, width=20,
      state='disabled')
    # Screens button
    self.screens_frame = tk.Frame(self.other_options_frame)
    self.screens_var = tk.BooleanVar()
    self.screens_text = tk.Label(self.screens_frame, text="    Screens?",
      state='disabled')
    self.screens_button = tk.Checkbutton(self.screens_frame, state='disabled',
      variable=self.screens_var)
    self.screens_info = Pmw.Balloon(self.other_options_frame)
    self.screens_info.bind(self.screens_button,
      "Reflect, Light Screen, or Aurora Veil")
    # Extend widgets list
    self.dmg_calc_widgets.extend([self.opp_abil_text, self.opp_abil_box,
      self.screens_text, self.screens_button])
    # Enter and clear buttons
    self.enter_button = tk.Button(self.window, text="Search", width=22,
      command=self.search_and_calc)
    self.enter_info = Pmw.Balloon(self.window)
    self.enter_info.bind(self.enter_button, "Keyboard shortcut: Enter")
    self.window.bind('<Return>', self.press_return)
    self.clear_button = tk.Button(self.window, text="Clear entries",
      command=self.clear_all_entries)
    # Tool output
    self.output_widget = tk.Text(self.window, state='disabled', height=48,
      width=len(SEP_LINE), wrap=tk.WORD, font=('Courier', 8))
    self.output_scroll = tk.Scrollbar(self.window)
    self.output_widget.config(yscrollcommand=self.output_scroll.set)
    self.output_scroll.config(command=self.output_widget.yview)

  def display(self):
    """Displays the object's widgets."""
    # Config summary
    self.config_options_text.grid(row=0, columnspan=4)
    # Trainer section
    self.trainer_box.focus_set()
    self.trainer_text.grid(row=1, column=0, sticky='e')
    self.trainer_box.grid(row=1, column=1, columnspan=3, sticky='w')
    # Opponent pokes
    self.poke_text.grid(row=2, column=0, sticky='e')
    self.poke_box.grid(row=2, column=1, columnspan=3, sticky='w')
    # Damage calculation button
    self.dmg_calc_frame.grid(row=3, column=0, columnspan=3)
    self.dmg_calc_text.grid(row=0, column=0, sticky='w')
    self.dmg_calc_button.grid(row=0, column=1, sticky='w')
    # Opponent level (Pt/HGSS) or nature (DP)
    if self.game == 'dp':
      self.opp_nature_frame.grid(row=3, column=3)
      self.opp_nature_text.grid(row=0, column=0)
      self.opp_nature_box.grid(row=0, column=1)
    elif self.game == 'pt':
      self.opp_lvl_frame.grid(row=3, column=3)
      self.opp_lvl_text.grid(row=0, column=0)
      self.opp_lvl_box.grid(row=0, column=1)
    # User's poke
    self.my_poke_text.grid(row=4, column=0, sticky='e')
    self.my_poke_box.grid(row=4, column=1, sticky='w')
    self.my_poke_att_frame.grid(row=4, column=2, columnspan=2)
    self.my_poke_att_text.pack(side='left')
    self.my_poke_att_button1.pack(side='left')
    self.my_poke_att_button2.pack(side='left')
    # Move name
    self.move_text.grid(row=5, column=0, sticky='e')
    self.move_box.grid(row=5, column=1, sticky='w')
    self.move_frame.grid(row=5, column=2, columnspan=2)
    # Z-Move or Max Move button
    if self.game == 'swsh':
      self.max_move_text.pack(side='left')
      self.max_move_button.pack(side='left')
    else:
      self.z_move_text.pack(side='left')
      self.z_move_button.pack(side='left')
    # Critical hit button
    self.crit_text.pack(side='left')
    self.crit_button.pack(side='left')
    # Attacker and defender stat boosts
    self.att_boosts_text.grid(row=6, column=0, sticky='e')
    self.boosts_frame.grid(row=6, column=1, columnspan=3, sticky='w')
    self.att_boosts_box.pack(side='left')
    self.def_boosts_text.pack(side='left')
    self.def_boosts_box.pack(side='left')
    # Field conditions: weather and terrain
    self.weather_text.grid(row=7, column=0, sticky='e')
    self.field_frame.grid(row=7, column=1, columnspan=3, sticky='w')
    self.weather_frame.grid(row=0, column=0)
    self.weather_box.pack(side='left')
    self.weather_scroll.pack(side='right')
    self.terrain_text.grid(row=0, column=1)
    self.terrain_frame.grid(row=0, column=2)
    self.terrain_box.pack(side='left')
    self.terrain_scroll.pack(side='right')
    # Attacker and defender status conditions
    self.att_status_text.grid(row=8, column=0, sticky='e')
    self.status_frame.grid(row=8, column=1, columnspan=3, sticky='w')
    self.att_status_box.grid(row=0, column=0, sticky='e')
    self.att_status_scroll.grid(row=0, column=1)
    self.def_status_text.grid(row=0, column=2)
    self.def_status_box.grid(row=0, column=3)
    self.def_status_scroll.grid(row=0, column=4)
    # Opponent's ability
    self.opp_abil_text.grid(row=9, column=0, sticky='e')
    self.other_options_frame.grid(row=9, column=1, columnspan=4, sticky='w')
    self.opp_abil_box.grid(row=0, column=1, sticky='w', columnspan=2)
    # Screens button
    self.screens_frame.grid(row=0, column=4, columnspan=1, sticky='w')
    self.screens_text.pack(side='left')
    self.screens_button.pack(side='right')
    # Enter and clear buttons
    self.enter_button.grid(row=10, column=0, columnspan=3)
    self.clear_button.grid(row=10, column=3)
    # Tool output
    self.output_widget.grid(row=0, column=5, rowspan=12)
    self.output_scroll.grid(row=0, column=6, rowspan=12, sticky='ns')

  def toggle_damage_calculation(self, _event=None):
    """Enables or disables all widgets related to damage calculation."""
    if self.dmg_calc_var.get():
      state = 'normal'
      self.enter_button['text'] = "Search and calculate"
    else:
      state = 'disabled'
      self.enter_button['text'] = "Search"
    for w in self.dmg_calc_widgets:
      w.config(state=state)
    # Max move / Z-move button
    if self.game == 'swsh':
      self.max_move_text.config(state=state)
      self.max_move_button.config(state=state)
    elif self.game in ('sm', 'usum'):
      self.z_move_text.config(state=state)
      self.z_move_button.config(state=state)
    else:
      self.z_move_text.config(state='disabled')
      self.z_move_button.config(state='disabled')
    # Terrains
    if self.generation < 6:
      self.terrain_text.config(state='disabled')
      self.terrain_box.config(state='disabled')
    # Ability
    if self.generation < 3:
      self.opp_abil_text.config(state='disabled')
      self.opp_abil_box.config(state='disabled')

  def clear_all_entries(self, _event=None):
    """Resets every widget to its default value."""
    self.trainer_box.delete(0, tk.END)
    self.poke_box.delete(0, tk.END)
    self.dmg_calc_var.set(False)
    self.opp_lvl_var.set(self.level)
    self.opp_nature_box.delete(0, tk.END)
    self.my_poke_att_var.set(True)
    self.z_move_var.set(False)
    self.max_move_var.set(False)
    self.crit_var.set(False)
    self.att_boosts_var.set("0")
    self.def_boosts_var.set("0")
    self.weather_box.see(0)
    self.terrain_box.see(0)
    self.att_status_box.see(0)
    self.def_status_box.see(0)
    self.screens_var.set(False)
    if not self.dmg_calc_var.get():
      # These widgets need to be enabled to allow clearing
      for wid in (self.my_poke_box, self.move_box, self.opp_abil_box):
        wid.config(state='normal')
        wid.delete(0, tk.END)
        wid.config(state='disabled')
    self.toggle_damage_calculation(_event)
    self.trainer_box.focus_set()

  def press_return(self, _event=None):
    """Event that happens when pressing Return: same as clicking Search."""
    self.enter_button.config(relief=tk.SUNKEN)
    self.enter_button.after(100,
      lambda: self.enter_button.config(relief=tk.RAISED))
    self.search_and_calc(_event)

  def search_and_calc(self, _event=None):
    """Runs the tool."""
    # Correct possibly wrong opponent level
    opp_lv = self.opp_lvl_box.get()
    if not is_valid_num(opp_lv) or int(opp_lv) < 10 or int(opp_lv) > 100:
      self.opp_lvl_var.set(self.level)
    # Run tool
    self.interface_obj.run(
      trainer_input=self.trainer_box.get(),
      pokes_input=self.poke_box.get(),
      do_damage_calc=self.dmg_calc_var.get(),
      my_poke_input=self.my_poke_box.get(),
      my_poke_is_attacker=self.my_poke_att_var.get(),
      move=self.move_box.get(),
      is_z_move=self.z_move_var.get(),
      is_max_move=self.max_move_var.get(),
      crit=self.crit_var.get(),
      att_boosts=self.att_boosts_box.get(),
      def_boosts=self.def_boosts_box.get(),
      weather=self.weather_box.get(self.weather_box.nearest(0)),
      terrain=self.terrain_box.get(self.terrain_box.nearest(0)),
      att_status=self.att_status_box.get(self.att_status_box.nearest(0)),
      def_status=self.def_status_box.get(self.def_status_box.nearest(0)),
      opp_ability=self.opp_abil_box.get(),
      screens=self.screens_var.get(),
      my_level=self.level,
      opp_level=self.opp_lvl_box.get(),
      opp_nature=self.opp_nature_box.get()
    )
    # Output text
    self.output_widget.config(state='normal')
    self.output_widget.delete('1.0', tk.END)
    self.output_widget.insert(tk.INSERT, self.interface_obj.output_buffer)
    self.output_widget.config(state='disabled')
