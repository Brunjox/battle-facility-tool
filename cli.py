﻿from configobj import ConfigObj
from src import tool
from src.utils import *

# Change the variables below to run the tool on the command line
trainer_input = ""
pokes_input = "a"
do_damage_calc = True

my_poke_input = "mymence"
my_poke_is_attacker = True
move = "pound"
is_z_move = False
is_max_move = False
crit = False
att_boosts = 0
def_boosts = 0
weather = ""
terrain = ""
att_status = ""
def_status = ""
opp_ability = ""
screens = False
my_level = 50
opp_level = my_level
opp_nature = ""

game = "pt"
lang = "eng"

# Run the tool
if __name__ == '__main__':
  interface_obj = tool.ToolInterfaceClass(game, lang)
  interface_obj.run(trainer_input, pokes_input, do_damage_calc, my_poke_input,
    my_poke_is_attacker, move, is_z_move, is_max_move, crit, att_boosts,
    def_boosts, weather, terrain, att_status, def_status, opp_ability, screens,
    my_level, opp_level, opp_nature)
  print(interface_obj.output_buffer)
