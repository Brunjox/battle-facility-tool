# Battle Facility Tool by Brunjox
Pokémon searching and damage calculations -- now in multiple languages!

## 1. Requirements
* **Python** is required: you can download it from https://python.org
  * You may want to [add Python to the PATH](https://duckduckgo.com/?t=ffab&q=add+python+to+path) so that it is easily accessible via the `python` command (or `python.exe`, depending on your operating system)
* **Python libraries** needed: install them with the following command:
```
python -m pip install configobj Pmw python-dict-wrapper tk
```
* **Node.js** is required to run the Smogon damage calculator, which you can install from https://nodejs.org (skip this step if you don't need it)


## 2. Usage
* Edit `CONFIG.txt` with the desired settings: game, language of Trainer names, and level (when applicable)
* Launch the tool with Python by running the following command while in the library folder:
```
python main.py
```
* In Windows, you can use `pythonw` instead of `python` to launch the tool without the terminal hanging; you can also create a shortcut for easy access
* Hover over widgets to get an explaination of what they do
* Write your own Pokémon in `my_pokemon.csv` to use them in damage calculations: `.csv` files can be opened either as spreadsheets (Excel, LibreOffice Calc, etc) or text files
  * You can either enter its 6 IVs separated by `/`, a single value which will be used for all 6, or leave the cell blank which gives the default value of all 31s
  * For Generation 2 Pokémon, use IVs and EVs column for DVs and stat experience instead; order of insertion is: hp, atk, def, spe (Speed), spa (Special)
* If clicking the search button does nothing, it's likely that an error has been raised; please check that you entered correct parameters (such as no Pokémon from incorrect Generations etc)
* For experienced users/programmers:
  * This tool implements some useful generic resources for Pokémon in Python, including interface with the JavaScript Smogon damage calculator and spreadsheets for Pokémon base stats in all generations
  * `cli.py` is the command-line interface of the same tool


## 3. TODOS (contributions are welcome!)
* !!! Battle Factory tools
* in `factory_main.py`, change `SearchLib` to get database name instead of game name
* fix debug width thingy
* 11 EV spreads are unknown or reported incorrectly by all sources, therefore they are set to all 0s in the database files:
  * DP: Stunky-1, Grotle-1, Shuckle-1, Steelix-1
  * Pt/HGSS: Flareon-H
  * BW: Ferroseed-1, Trubbish-1, Gothorita-1, Electrode-2, Muk-3, Magmortar-3
* Gen 2: implement exact damage formula
* Gen 3: everything about Ruby/Sapphire's Battle Tower
* Gen 3: add all foreign names (and check for duplicates!)
* Gen 4: Pt/HGSS ambiguous names: Maggie or Maggy? Rocky or Rockie?


## 4. Licensing
This tool is to be distributed under BSD 3-Clause License (see `LICENSE.txt` for details)


## 5. Sources: databases
### Gen 2
* Spreadsheet by SadisticMystic: https://docs.google.com/spreadsheets/d/1QKSFElVwuWrGIAPIZbGzzQS657ZJWL1wlUlHaVz_eBc

### Gen 3
Emerald:
* General FAQ by Werster (links to other FAQs): https://pastebin.com/c796fkZT
* IV information by Werster: https://pastebin.com/JFbqA0PY
* Spreadsheet by Hozu: https://dropbox.com/s/qaujn3plwdgfadc/EmeraldBattleFrontierComplete.xlsx
  * Note: here Shuckle-5 (Lucy 1) is stated to have the EV Spread 252/0/0/106/252/0; this has been corrected to 252/0/0/6/252/0
* Factory info by Hozu: https://smogon.com/forums/threads/battle-frontier-max-stats-pokemon-database.15426/post-4481019

### Gen 4
DP:
* Pokémon sets by Jumpman16: https://smogon.com/ingame/bc/dp_bt_pokemon
* Trainers and IV information: https://bulbapedia.bulbagarden.net/wiki/List_of_Battle_Tower_Trainers_(Generation_IV)
* Trainer rosters: https://pokebook.jp/data/sp4/bt.html
Pt/HGSS:
* Frontier Pokémon by Jumpman16: https://smogon.com/forums/threads/pokemon-platinum-the-definitive-thread-mark-5-battle-frontier-discussion.45802/post-1489122
* Trainers 1-200 rosters by myself (manual check with code assistance): https://bulbapedia.bulbagarden.net/wiki/List_of_Battle_Frontier_Trainers_(Generation_IV) and pages linked therein
* Trainers 201-300 rosters by Level 51: https://docs.google.com/spreadsheets/d/1S7908O9U44xiJCTaE5IDUdHcCzqn01r8U1bLSTNTiMs
* Hall Pokémon by Peterko: https://smogon.com/forums/threads/platinum-hg-ss-battle-frontier-and-dp-battle-tower-records.52858/post-1850704
* General FAQ by Werster (links to other FAQs): https://pastebin.com/ZjaFtEkj
* IV information by Werster: https://pastebin.com/XPxEtG9f
* Thorton roster and IV information by atsync: https://smogon.com/forums/threads/4th-generation-battle-facilities-discussion-and-records.3663294/post-8536093
* Other Frontier Brains sets: Brains' individual pages from https://bulbapedia.bulbagarden.net/wiki/Battle_Frontier_(Generation_IV)#Facilities
* Backup source: https://docs.google.com/spreadsheets/d/1uU_MuXklN33Pc47CZe3QHpP_OcLR9YqSoJCDfZBamiw

### Gen 5
* Pokémon sets by Team Rocket Elite: http://members.shaw.ca/teamrocketelite/BattleSubwayData6.txt (accessed via Wayback Machine at https://web.archive.org/web/20130510005942/http://members.shaw.ca/teamrocketelite/BattleSubwayData6.txt)
* Trainer rosters by Team Rocket Elite: http://members.shaw.ca/teamrocketelite/BattleSubwayTrainers.txt (accessed via Wayback Machine at https://web.archive.org/web/20161206214518/http://members.shaw.ca/teamrocketelite/BattleSubwayTrainers.txt)
* Trainer names: https://bulbapedia.bulbagarden.net/wiki/List_of_Battle_Subway_Trainers
* Backup source: https://smogon.com/forums/threads/black-white-battle-subway-records-now-with-gen-4-records.102593/post-4177645

### Gen 6
* Pokémon sets by Team Rocket Elite: http://members.shaw.ca/teamrocketelite/BattleMaisonData.txt accessed via Wayback Machine at https://web.archive.org/web/20150502162137/http://members.shaw.ca/teamrocketelite/BattleMaisonData.txt)
* Trainer rosters by Team Rocket Elite: http://members.shaw.ca/teamrocketelite/BattleMaisonTrainers.txt (accessed via Wayback Machine at https://web.archive.org/web/20150105221545/http://members.shaw.ca/teamrocketelite/BattleMaisonTrainers.txt)
* Trainer names: https://bulbapedia.bulbagarden.net/wiki/List_of_Battle_Maison_Trainers

### Gen 7
* Spreadsheet by Team Rocket Elite: https://docs.google.com/spreadsheets/d/1D7T0jCHBkDppjTB0_rJyK0houk8zE3wLmZ1DlVXEkXg
* Trainer names: https://bulbapedia.bulbagarden.net/wiki/List_of_Battle_Tree_Trainers
* Set differences between SM and USUM: https://bulbapedia.bulbagarden.net/wiki/Battle_Tree/Pok%C3%A9mon

### Gen 8
* Spreadsheet by SadisticMystic: https://docs.google.com/spreadsheets/d/15r_-iUngMDoA9ux7tCkUJLJVBeQhpIzEH7rju9fgWOQ

### Miscellaneous
* Damage calculator source code by Smogon: https://github.com/smogon/damage-calc
* All foreign Trainer names: https://bulbapedia.bulbagarden.net/wiki/User:Abcboy#Text_dumps


## 6. Sources: original research
Method: comparing HP values before and after using Pain Split

### Gen 4
DP:
* Trainers 201-220: 99%-surely 21 IV
* Trainers 1-7: 99%-surely 3 IV
  * -> I *assume* that ones in between go up by 3, just like in Platinum
* Palmer 1: 99%-surely 30-31 IV
  * -> I *assume* this is also the case for Palmer 2
Pt/HGSS:
* Non-Thorton Frontier Brains fights 1: 99%-surely 30-31 IV
  * -> I *assume* this is also the case for fights 2
* I also *assume* that Pt is the same (testing was done in HGSS)

### Gen 5
* Trainers 1-200: 99%-surely they start from 3 IV and go up by 3, like in the previous generation
* In particular, 99%-surely the blocks are: 1-50 (3), 51-70 (6), 71-90 (9), 91-110 (12), 111-160 (15), 161-180 (18), 181-200 (21), 201-300 + bosses (31)
* I *assume* that B2W2 is the same (testing was done in BW)

### Gen 6
* Trainers 1-110: 99%-surely they start from 3 IV and go up by 4
* In particular, 99%-surely the blocks are: 1-32 (3), 33-50 (3), 51-70 (6-7), 71-90 (11), 91-110 (15)
* I *assume* that XY is the same (testing was done in ORAS)
