from src import factory_tool

level = 100
input_ = "8"

gen = 3
lang = 'eng'

if __name__ == '__main__':
  tool = factory_tool.BattleFactoryToolInterfaceClass(gen, level, lang)
  tool.get_roster(input_)
